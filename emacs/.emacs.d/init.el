;test
  (setq inhibit-startup-message t)

  (if (display-graphic-p)
    (progn
    (scroll-bar-mode -1)          ;Disable visible scrolling
    (tool-bar-mode -1)
    (tooltip-mode -1))
    (set-fringe-mode 10))

  (setq visible-cursor nil)

  (menu-bar-mode -1)

  (setq visible-bell t)         ;this flashes on backspace

  ;(set-face-attribute 'default nil :font "SourceCode Pro" :height 120)
  (set-face-attribute 'default nil :font "Iosevka" :height 140)
  ;(add-to-list 'default-frame-alist '(font . "Iosevka" ))
  ;(set-face-attribute 'default t :font "Iosevka" )

  ;(load-theme 'wombat)

  ;; Make ESC quit prompts
  (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

  ;; Initialize package sources
  (require 'package)

  (setq package-archives '(("melpa" . "https://melpa.org/packages/")
                           ("org" . "https://orgmode.org/elpa/")
                           ("elpa" . "https://elpa.gnu.org/packages/")))

  (package-initialize)
  (unless package-archive-contents
   (package-refresh-contents))

  ;; Initialize use-package on non-Linux platforms
  (unless (package-installed-p 'use-package)
     (package-install 'use-package))

  (require 'use-package)
  (setq use-package-always-ensure t)

  (custom-set-variables
   ;; custom-set-variables was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(ansi-color-names-vector
     ["#292D3E" "#ff5370" "#c3e88d" "#ffcb6b" "#82aaff" "#c792ea" "#89DDFF" "#EEFFFF"])
   '(custom-safe-themes
     '("47db50ff66e35d3a440485357fb6acb767c100e135ccdf459060407f8baea7b2" "028c226411a386abc7f7a0fba1a2ebfae5fe69e2a816f54898df41a6a3412bb5" "e8df30cd7fb42e56a4efc585540a2e63b0c6eeb9f4dc053373e05d774332fc13" "da186cce19b5aed3f6a2316845583dbee76aea9255ea0da857d1c058ff003546" "234dbb732ef054b109a9e5ee5b499632c63cc24f7c2383a849815dacc1727cb6" default))
   '(exwm-floating-border-color "#232635")
   '(fci-rule-color "#676E95")
   '(global-command-log-mode t)
   '(highlight-tail-colors ((("#383f45") . 0) (("#323e51") . 20)))
   '(jdee-db-active-breakpoint-face-colors (cons "#1c1f2b" "#c792ea"))
   '(jdee-db-requested-breakpoint-face-colors (cons "#1c1f2b" "#c3e88d"))
   '(jdee-db-spec-breakpoint-face-colors (cons "#1c1f2b" "#676E95"))
   '(objed-cursor-color "#ff5370")
   '(package-selected-packages
     '(chord key-chord buffer-flip moom org-tree-slide org-tree-slide-mode org-tree-slide-mod org-bullets forge evil-magit magit visual-fill-column nov openwith counsel-projectile projectile pdf-tools evil-collection evil general doom-themes helpful counsel ivy-rich which-key rainbow-delimiters ivy command-log-mode use-package xclip org))
   '(pdf-view-midnight-colors (cons "#EEFFFF" "#292D3E"))
   '(rustic-ansi-faces
     ["#292D3E" "#ff5370" "#c3e88d" "#ffcb6b" "#82aaff" "#c792ea" "#89DDFF" "#EEFFFF"])
   '(vc-annotate-background "#292D3E")
   '(vc-annotate-color-map
     (list
      (cons 20 "#c3e88d")
      (cons 40 "#d7de81")
      (cons 60 "#ebd476")
      (cons 80 "#ffcb6b")
      (cons 100 "#fcb66b")
      (cons 120 "#f9a16b")
      (cons 140 "#f78c6c")
      (cons 160 "#e78e96")
      (cons 180 "#d690c0")
      (cons 200 "#c792ea")
      (cons 220 "#d97dc1")
      (cons 240 "#ec6898")
      (cons 260 "#ff5370")
      (cons 280 "#d95979")
      (cons 300 "#b36082")
      (cons 320 "#8d678b")
      (cons 340 "#676E95")
      (cons 360 "#676E95")))
   '(vc-annotate-very-old-color nil))
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   )

  ;;Disable line numbers for some modes
  (require 'display-line-numbers)

  (defcustom display-line-numbers-exempt-modes
    '(vterm-mode eshell-mode shell-mode term-mode ansi-term-mode nov-mode org-mode treemacs-mode-hook)
    "Major modes on which to disable line numbers."
    :group 'display-line-numbers
    :type 'list
    :version "green")

  (defun display-line-numbers--turn-on ()
    "Turn on line numbers except for certain major modes.
  Exempt major modes are defined in `display-line-numbers-exempt-modes'."
    (unless (or (minibufferp)
                (member major-mode display-line-numbers-exempt-modes))
      (display-line-numbers-mode)))

  (global-display-line-numbers-mode)

(use-package buffer-flip
  :ensure t
  :bind  (("M-<tab>" . buffer-flip)
          :map buffer-flip-map
          ( "M-<tab>" .   buffer-flip-forward) 
          ( "M-1" . buffer-flip-backward) 
          ( "M-ESC" .     buffer-flip-abort))
  :config
  (setq buffer-flip-skip-patterns
        '("^\\*helm\\b"
          "^\\*swiper\\*$")))

(global-set-key (kbd "C-M-j") 'counsel-switch-buffer) ;switch buffers

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :config
  (setq ivy-initial-inputs-alist nil)); Don't start searches with ^

(use-package command-log-mode)

(use-package ivy :ensure t
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)	
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

(use-package which-key :ensure t
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.3))

(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(when (display-graphic-p)
  (require 'all-the-icons))

(require 'doom-modeline)
(doom-modeline-mode 1)
(use-package doom-themes)
;(load-theme 'doom-palenight)
(use-package flatui-theme)
(use-package flatui-dark-theme)
(use-package poet-theme)
;(modus-themes-load-vivendi)
(load-theme 'doom-badger)
;(load-theme 'poet)


(use-package rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package general
  :config
  (general-create-definer rune/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")

  (rune/leader-keys
    "a" '(:ignore t :which-key "agenda")
    "aa" '(org-agenda :which-key "org-agenda")

    "m" '(mu4e :which-key "mu4e")

    "t"  '(:ignore t :which-key "toggles")
    "tt" '(counsel-load-theme :which-key "choose theme")

    "v" '(:ignore t :which-key "pdf-mode")
    "vv" '(pdf-view-mode :which-key "pdf-view-mode")
    "vr" '(pdf-view-rotate-clockwise :which-key "pdf-rotate-clockwise")

    "r" '(:ignore t :which-key "update/refresh")
    "rr" '(elfeed-update :which-key "update-feed")
    "rl" '(eww-follow-link :which-key "follow link")
    ))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
                                        ;(define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(rune/leader-keys
  "ts" '(hydra-text-scale/body :which-key "scale text"))

(use-package nov) ;read epubs
(setq nov-text-width 120)
(use-package visual-fill-column)
                                        ;(setq nov-text-width t)
(setq-default visual-fill-column-center-text t)
(add-hook 'nov-mode-hook 'visual-line-mode)
(add-hook 'nov-mode-hook 'visual-fill-column-mode)

(add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))

(setq split-width-threshold nil) ;so it doesn't split the wrong way or something
                                        ; COMPILE RESIZ
(defun my-compile ()
  "Run compile and resize the compile window"
  (interactive)
  (progn
    (call-interactively 'compile)
    (setq cur (selected-window))
    (setq w (get-buffer-window "*compilation*"))
    (select-window w)
    (setq h (window-height w))
    (shrink-window (- h 12))
    (select-window cur)
    )
  )

(defun my-compilation-hook () 
  "Make sure that the compile window is splitting vertically"
  (progn
    (if (not (get-buffer-window "*compilation*"))
        (progn
          (split-window-vertically 10)
          )
      )
    )
  )

(add-hook 'compilation-mode-hook 'my-compilation-hook)
(global-set-key [f9] 'my-compile)

; disabled because of agenda keybinding
; maybe disable in agenda mode or something
;(windmove-default-keybindings)
;(setq windmove-wrap-around t)

(setq TeX-PDF-mode t)
  (use-package pdf-tools)
  ; enable rotate in pdf-tools
  ; requires pdftk
  (defun pdf-view--rotate (&optional counterclockwise-p page-p)
  "Rotate PDF 90 degrees.  Requires pdftk to work.\n
Clockwise rotation is the default; set COUNTERCLOCKWISE-P to
non-nil for the other direction.  Rotate the whole document by
default; set PAGE-P to non-nil to rotate only the current page.
\nWARNING: overwrites the original file, so be careful!"
  ;; error out when pdftk is not installed
  (if (null (executable-find "pdftk"))
      (error "Rotation requires pdftk")
    ;; only rotate in pdf-view-mode
    (when (eq major-mode 'pdf-view-mode)
      (let* ((rotate (if counterclockwise-p "left" "right"))
             (file   (format "\"%s\"" (pdf-view-buffer-file-name)))
             (page   (pdf-view-current-page))
             (pages  (cond ((not page-p)                        ; whole doc?
                            (format "1-end%s" rotate))
                           ((= page 1)                          ; first page?
                            (format "%d%s %d-end"
                                    page rotate (1+ page)))
                           ((= page (pdf-info-number-of-pages)) ; last page?
                            (format "1-%d %d%s"
                                    (1- page) page rotate))
                           (t                                   ; interior page?
                            (format "1-%d %d%s %d-end"
                                    (1- page) page rotate (1+ page))))))
        ;; empty string if it worked
        (if (string= "" (shell-command-to-string
                         (format (concat "pdftk %s cat %s "
                                         "output %s.NEW "
                                         "&& mv %s.NEW %s")
                                 file pages file file file)))
            (pdf-view-revert-buffer nil t)
          (error "Rotation error!"))))))

(defun pdf-view-rotate-clockwise (&optional arg)
  "Rotate PDF page 90 degrees clockwise.  With prefix ARG, rotate
entire document."
  (interactive "P")
  (pdf-view--rotate nil (not arg)))

(defun pdf-view-rotate-counterclockwise (&optional arg)
  "Rotate PDF page 90 degrees counterclockwise.  With prefix ARG,
rotate entire document."
  (interactive "P")
  (pdf-view--rotate :counterclockwise (not arg)))
  (add-to-list 'auto-mode-alist '("\\.pdf\\'" . fundamental-mode)) ;disable docview for pdfs

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/Projects/Code")
    (setq projectile-project-search-path '("~/Projects/Code")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(setq magit-visit-ref-behavior '(checkout-any focus-on-ref))

(use-package evil-collection
  :after magit)

(evil-collection-init)

(setq auth-sources '("~/.authinfo")) ;api key from gitlab
(use-package forge) ;magit extension

;for rss feeds
;; Mark all YouTube entries
(use-package elfeed
  :hook (elfeed-show-mode . efs/org-mode-visual-fill)
  :hook (elfeed-search-mode . efs/org-mode-visual-fill)
  :commands elfeed-make-tagger
  :init
  (add-hook 'elfeed-new-entry-hook
        (elfeed-make-tagger :feed-url "subsplease\\.org" :add '(anime))
        )

  )
(global-set-key (kbd "C-c w") 'elfeed)
(setq elfeed-feeds
      '("https://planet.emacslife.com/atom.xml"
        "https://subsplease.org/rss/?r=1080"

        ))
(setq-default elfeed-search-filter "@1-week-ago +unread -anime")

(use-package ssh)
(lambda ()
  (shell-dirtrack-mode t)
  (setq dirtrackp nil))

; org config
(defun efs/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (visual-line-mode 1))

(defun efs/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  ;; Set faces for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(use-package org
  :hook (org-mode . efs/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")

  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

  (setq org-agenda-files
	'("~/Projects/Org/Tasks.org"
	"~/Projects/Org/Birthdays.org"))
  (efs/org-font-setup))

(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))

(org-babel-do-load-languages
                 'org-babel-load-languages
                 '((emacs-lisp . t)
                   (python . t)))
(setq org-confirm-babel-evaluate nil)

(push '("conf-unix" . conf-unix) org-src-lang-modes)

;; Automatically tangle our Emacs.org config file when we save it
(defun efs/org-babel-tangle-config ()
  (when (string-equal (buffer-file-name)
		      (expand-file-name "/home/wyatt/Projects/dotfiles/emacs/.emacs.d/Emacs.org"))
    ;; Dynamic scoping to the rescue
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'efs/org-babel-tangle-config)))

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 140
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . efs/org-mode-visual-fill))


  (setq org-tag-alist
    '((:startgroup)
       ; Put mutually exclusive tags here
       (:endgroup)
       ("@hw" . ?h)
       ("@study" . ?s)
       ("note" . ?n)
       ("idea" . ?i)))

  (setq org-refile-targets
    '(("Archive.org" :maxlevel . 1)
      ("Tasks.org" :maxlevel . 1)))

  (advice-add 'org-refile :after 'org-save-all-org-buffers)

(use-package org-tree-slide
  :custom
  (org-image-actual-width nil))
(use-package moom)
(with-eval-after-load "moom"
  ;; add settings here ...
  (moom-mode 1))

(define-key org-mode-map (kbd "<f8>") 'org-tree-slide-mode)
(define-key org-mode-map (kbd "S-<f8>") 'moom-toggle-frame-maximized)

(setq org-tree-slide-slide-in-effect nil)
(setq org-tree-slide-fold-subtrees-skipped t)

(when (require 'org-tree-slide nil t)
  (global-set-key (kbd "<f8>") 'org-tree-slide-mode)
  (global-set-key (kbd "S-<f8>") 'org-tree-slide-skip-done-toggle)
  (define-key org-tree-slide-mode-map (kbd "<f9>")
    'org-tree-slide-move-previous-tree)
  (define-key org-tree-slide-mode-map (kbd "<f10>")
    'org-tree-slide-move-next-tree)
  (define-key org-tree-slide-mode-map (kbd "<f11>")
    'org-tree-slide-content)
  (setq org-tree-slide-skip-outline-level 4)
  (org-tree-slide-narrowing-control-profile)
  (setq org-tree-slide-skip-done nil))

(with-eval-after-load "org-tree-slide"
  (defvar my-hide-org-meta-line-p nil)
  (defun my-hide-org-meta-line ()
    (interactive)
    (setq my-hide-org-meta-line-p t)
    (set-face-attribute 'org-meta-line nil
			                  :foreground (face-attribute 'default :background)))
  (defun my-show-org-meta-line ()
    (interactive)
    (setq my-hide-org-meta-line-p nil)
    (set-face-attribute 'org-meta-line nil :foreground nil))

  (defun my-toggle-org-meta-line ()
    (interactive)
    (if my-hide-org-meta-line-p
	      (my-show-org-meta-line) (my-hide-org-meta-line)))

  (add-hook 'org-tree-slide-play-hook #'my-hide-org-meta-line)
  (add-hook 'org-tree-slide-stop-hook #'my-show-org-meta-line))

(setq inhibit-splash-screen t)
(org-agenda-list)
(delete-other-windows)

(defun efs/lsp-mode-setup()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  ; open treemacs and symbols in pane when launched
  ;:hook (lsp-mode . treemacs)
  ;:hook (lsp-mode . lsp-treemacs-symbols)
  :custom
  (lsp-ui-doc-position 'bottom))

(use-package lsp-treemacs
  :after lsp)
(lsp-treemacs-sync-mode 1)

; lsp-ivy-workspace-symbol
  (use-package lsp-ivy)

(use-package typescript-mode
  :mode "\\.ts\\'"
  :hook (typescript-mode . lsp-deferred)
  :config
  (setq typescript-indent-level 2))

(use-package js2-mode)
(use-package skewer-mode)
(use-package simple-httpd)
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-to-list 'interpreter-mode-alist '("node" . js2-mode))
(add-hook 'js2-mode-hook 'skewer-mode)
(add-hook 'css-mode-hook 'skewer-css-mode)
(add-hook 'html-mode-hook 'skewer-html-mode)

(use-package zig-mode)

(use-package slime)
(setq inferior-lisp-program "sbcl")

(use-package processing-mode)
(setq processing-location "/usr/bin/processing-java")

(setq lsp-pylsp-plugins-pydocstyle-enabled nil) ;remove docstyle warnings
(use-package lsp-mode
  :init
  (setq lsp-keymap-prefix "C-c l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (python-mode . lsp)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp)

(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(require 'mu4e-context)  ;; support for contexts
(use-package mu4e
  :ensure nil
   :load-path "/usr/share/emacs/site-lisp/mu4e/"
   ;:defer 5 ; Wait until 5 seconds after startup
  :config

  ;; This is set to 't' to avoid mail syncing issues when using mbsync
  (setq mu4e-change-filenames-when-moving t)

  ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-maildir "~/Mail")

  (setq mu4e-contexts
      (list
       ;; Work account
       (make-mu4e-context
        :name "gmail"
        :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/Gmail" (mu4e-message-field msg :maildir))))
        :vars '((user-mail-address . "hortonwhy@gmail.com")
                (user-full-name    . "Wyatt Horton")
                (mu4e-drafts-folder  . "/Gmail/[Gmail]/Drafts")
                (mu4e-sent-folder  . "/Gmail/[Gmail]/Sent Mail")
                (mu4e-refile-folder  . "/Gmail/[Gmail]/All Mail")
                (mu4e-trash-folder  . "/Gmail/[Gmail]/Trash")))
              (make-mu4e-context
      :name "protonmail"
      :match-func
        (lambda (msg)
          (when msg
            (string-prefix-p "/Protonmail" (mu4e-message-field msg :maildir))))
      :vars '((user-mail-address . "hortonwhy@protonmail.com")
              (user-full-name    . "Wyatt Horton")
              (mu4e-drafts-folder  . "/Protonmail/Drafts")
              (mu4e-sent-folder  . "/Protonmail/Sent")
              (mu4e-refile-folder  . "/Protonmail/Archive")
              (mu4e-trash-folder  . "/Protonmail/Trash")))
       ))

  (setq mu4e-drafts-folder "/Gmail/[Gmail]/Drafts")
  (setq mu4e-sent-folder   "/Gmail/[Gmail]/Sent Mail")
  (setq mu4e-refile-folder "/Gmail/[Gmail]/All Mail")
  (setq mu4e-trash-folder  "/Gmail/[Gmail]/Trash")

  (setq mu4e-maildir-shortcuts
      '(("/Protonmail/All Mail"             . ?i)
        ("/Gmail/[Gmail]/Sent Mail" . ?s)
        ("/Protonmail/Sent" . ?t)
        ;("/Gmail/[Gmail]/Trash"     . ?t)
        ;("/Gmail/[Gmail]/Drafts"    . ?d)
        ;("/Gmail/[Gmail]/All Mail"  . ?a)
        )))
(require 'mu4e-context)  ;; support for contexts

;run mu4e in background to sync
  ;(mu4e t)
